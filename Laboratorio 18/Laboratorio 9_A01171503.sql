--Materiales(Clave, Descripci�n, Costo, PorcentajeImpuesto) 
--Proveedores(RFC, RazonSocial) 
--Proyectos(Numero, Denominacion) 
--Entregan(Clave, RFC, Numero, Fecha, Cantidad) 

--La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. 
SET DATEFORMAT dmy

SELECT SUM(Cantidad) AS 'Cantidades', SUM(Cantidad)*costo*((PorcentajeImpuesto*0.01)+1) AS 'Importe total'
FROM Entregan,Materiales
WHERE Entregan.Clave = Materiales.Clave AND Fecha BETWEEN '1/01/1997' and '31/12/1997'
GROUP BY PorcentajeImpuesto,Costo

--Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas.
Select RazonSocial AS 'Raz�n Social',count(Entregan.Numero) AS 'N�mero de entregas',SUM(Cantidad)*costo*((PorcentajeImpuesto*0.01)+1) AS 'Importe total'
FROM Proveedores,Entregan,Materiales
WHERE Entregan.Clave = Materiales.Clave and Proveedores.RFC = Entregan.RFC
group by PorcentajeImpuesto,Costo,RazonSocial
order by [Raz�n Social] asc

--Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, la m�nima cantidad entregada, 
--la m�xima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio 
--entregada sea mayor a 400.

SELECT Materiales.Clave,Descripcion,SUM(Cantidad) AS 'Cantidades',MIN(Cantidad) AS 'Cantidad minima entregada',
MAX(Cantidad) AS 'Cantidad m�xima entregada',SUM(Cantidad)*costo*((PorcentajeImpuesto*0.01)+1) AS 'Importe total'
FROM Materiales,Entregan
where Entregan.Clave = Materiales.Clave
group by PorcentajeImpuesto,Costo,Materiales.Clave,Descripcion
having AVG(Cantidad)>400


--Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, detallando la clave 
--y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500.

Select RazonSocial as 'Razon Social', AVG(Cantidad) AS 'Promedio de materiales',Materiales.Clave,Materiales.Descripcion
from Materiales,Proveedores,Entregan
where Materiales.Clave = Entregan.Clave and Proveedores.RFC = Entregan.RFC 
group by RazonSocial,Materiales.Clave,Materiales.Descripcion
Having AVG(Cantidad)>500

--Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos 
--grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para 
--los que la cantidad promedio entregada sea mayor a 450.

(Select RazonSocial as 'Razon Social', AVG(Cantidad) AS 'Promedio de materiales',Materiales.Clave,Materiales.Descripcion
from Materiales,Proveedores,Entregan
where Materiales.Clave = Entregan.Clave and Proveedores.RFC = Entregan.RFC 
group by RazonSocial,Materiales.Clave,Materiales.Descripcion
Having AVG(Cantidad)<370)
union
(Select RazonSocial as 'Razon Social', AVG(Cantidad) AS 'Promedio de materiales',Materiales.Clave,Materiales.Descripcion
from Materiales,Proveedores,Entregan
where Materiales.Clave = Entregan.Clave and Proveedores.RFC = Entregan.RFC 
group by RazonSocial,Materiales.Clave,Materiales.Descripcion
Having AVG(Cantidad)>450)
order by AVG(Cantidad) asc


--inserta cinco nuevos materiales.--
select*from Materiales
INSERT INTO Materiales VALUES (1440,'Plomo',500.00,3.1) ; 
INSERT INTO Materiales VALUES (1450,'Cal',100.00,2.52) ; 
INSERT INTO Materiales VALUES (1460,'Vidrio',200.00,2.13) ; 
INSERT INTO Materiales VALUES (1470,'Madera de Caoba',700.00,3.20) ; 
INSERT INTO Materiales VALUES (1480,'Madera de Chicle',800.00,3.25) ; 

--Clave y descripci�n de los materiales que nunca han sido entregados.--
select clave,descripcion from Materiales
where Clave NOT IN (select Clave from Entregan)

--Raz�n social de los proveedores que han realizado entregas tanto al 
--proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'. 
(Select razonsocial from Proveedores,Entregan,Proyectos
where Entregan.RFC = Proveedores.RFC and Entregan.Numero = Proyectos.Numero and Proyectos.Denominacion = 'Vamos Mexico')
intersect 
(Select razonsocial from Proveedores,Entregan,Proyectos
where Entregan.RFC = Proveedores.RFC and Entregan.Numero = Proyectos.Numero and Proyectos.Denominacion = 'Queretaro limpio')

--Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucat�n'. 
Select distinct descripcion from Materiales,Entregan,Proyectos
where Materiales.Clave = Entregan.Clave and entregan.Numero = Proyectos.Numero and Denominacion NOT IN (select denominacion from proyectos where Proyectos.Denominacion = 'CIT Yucat�n')

-- Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor al promedio de 
--la cantidad entregada por el proveedor con el RFC 'VAGO780901'. 

select RazonSocial, AVG(cantidad) as 'Cantidad'
from Proveedores,Entregan
where Entregan.RFC = Proveedores.RFC 
group by RazonSocial
having AVG(Cantidad) < (select SUM(Cantidad) from Proveedores,Entregan where Proveedores.rfc = entregan.RFC and Entregan.RFC = 'EEEE800101') 

--RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades 
--totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001

Select proveedores.RFC,RazonSocial,SUM(Cantidad) AS 'Cantidades' from Proveedores,Entregan,Proyectos
where Proveedores.RFC = Entregan.RFC and  Entregan.Numero = Proyectos.Numero and Proyectos.Denominacion = 'Infonavit Durango'
and Fecha between '1/01/2000' and '31/12/2000'
group by Proveedores.RFC,RazonSocial
having SUM(cantidad) < (SELECT SUM(cantidad) from Proveedores,Entregan,Proyectos where Proveedores.RFC = Entregan.RFC and  Entregan.Numero = Proyectos.Numero and Proyectos.Denominacion = 'Infonavit Durango' and Fecha between '1/01/2001' and '31/12/2001')