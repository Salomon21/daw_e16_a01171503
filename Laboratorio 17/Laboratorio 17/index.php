<?php
require_once('modelo.php');
session_start();
include('_header.html');
include('_section.html');
?>
	<h2 class="center-align" id="borde">Página del equipo</h2>
	<h5 align="justify" id="borde">En el siguiente apartado podrás consultar la opción que desees entre las que se muestran del equipo, es importante recordar que esta
información ha sido creada con finalidad puramente educativa.</h5>
	<h6 align="justify" id="borde">En el botón jugador podras ver los jugadores guardados y además tendrás la posibilidad de crear un nuevo jugador o eliminar uno existente, en peso solo se muestra una consulta de jugadores con peso mayor a 85kg y en ID verás a los jugadores y su ID.</h6>
	<br>
	<br>
	<div class="row">
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<a class="white-text" href="consulta1.php">
				<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
					<span>Jugadores</span>
				</button>
			</a>
		</div>
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<a class="white-text" href="consulta2.php">
				<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
					<span>Peso</span>
				</button>
			</a>
		</div>
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<a class="white-text" href="consulta3.php">
				<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
					<span>ID</span>
				</button>
			</a>
		</div>
	</div>
	<?php include ('_footer.html'); ?>