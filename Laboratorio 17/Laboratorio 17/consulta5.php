<?php

require_once('modelo.php');
include('_header.html');
include('_section.html');
?>
	<h2 class="center-align">Preguntas</h2>
	<br>
	<div class="container">
		<h5 class="center-align">1.- ¿Qué importancia tiene AJAX en el desarrollo de RIA's (Rich Internet Applications?</h5>
		<p align="justify">Es de alta importancia, ya que su uso permite la creación de aplicaciones más versátiles, faciles de usar y en general, más útiles e interactivas.</p>
		<h5 class="center-align">2.- ¿Qué implicaciones de seguridad tiene AJAX?</h5>
		<p align="justify"> Se deve tener cuidado con su manejo, ya que gran parte de la funcionalidad se lleva a cabo del lado del usuario, y es por esto mismo que las validacicones importantes aún deven ser hechas del lado del servidor, para evitar ataques como sql injection.</p>
		<h5 class="center-align">3.- ¿Dónde se deben hacer las validaciones de seguridad, del lado del cliente o del lado del servidor?</h5>
		<p align="justify"> JSON (JavaScript Object Notation), es un formmato de texto para el intercambio de datos, considerado un formato de lenguaje independiente. Es derivado de JavaScript y se usa como una alternativa más amigable de XML. </p>
	</div>
	<?php
include ('_footer.html');
?>