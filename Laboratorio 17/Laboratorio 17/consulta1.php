<?php

require_once('modelo.php');
session_start();
include('_header.html');
include('_section.html');
?>
	<?php if (isset($_SESSION["info"])){ echo $_SESSION["info"]; unset($_SESSION["info"]); } ?>
		<h2 class="center-align">Jugadores del equipo</h2>
		<br>
		<div class="row">
			<div class="col s4">&nbsp</div>
			<div class="col s4">
				<?php getJugadores(); ?>
			</div>
			<div class="col s4">&nbsp</div>
		</div>
		<div class="row">
			<div class="col s4">&nbsp</div>
			<div class="col s4">
				<a class="white-text" href="add.php">
					<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
						<span>Agregar</span>
					</button>
				</a>
			</div>
			<div class="col s4">&nbsp</div>
		</div>
		<?php
include ('_footer.html');
?>