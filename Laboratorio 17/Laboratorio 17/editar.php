<?php

require_once('modelo.php');
session_start();
include('_header.html');
include('_section.html');
if (isset($_POST["nombre"])) {
    guardar($_POST["nombre"], $_POST["apellido"], $_POST["peso"]);
	if(isset($_SESSION["error"])) {
		header("location: editar.php");
		die();
	}
    $_SESSION["info"] = "¡Jugador guardado!";
    header("location: consulta1.php");
} else  {
	include('_editar.html');
		}
include ('_footer.html');
?>