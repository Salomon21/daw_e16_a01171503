<?php
function connect_db(){
	$mysql = mysqli_connect ("localhost", "root","","hockey");
	return $mysql;
}
function close_db($mysql){
	mysqli_close($mysql);
}

function  getJugadores() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        // use of numeric index
        echo 'Jugador: '. $row[1]; 
        // name of the column as associative index
		echo ' ' .  $row['apellido'];
    	echo ' (' .  $row['peso'].' kg.)';
    	echo '<br /><br/>';
    }
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

function  getJugadoresA() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador WHERE peso>85';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        // use of numeric index
        echo 'Jugador: '. $row[1]; 
        // name of the column as associative index
		echo ' ' .  $row['apellido'];
    	echo ' (' .  $row['peso'].' kg.)';
    	echo '<br /><br/>';
    }
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

function  getJugadoresI() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        // use of numeric index
		echo ' ' .  $row['id'];
        echo ' '. $row[1]; 
		echo ' ' .  $row['apellido'];
        // name of the column as associative index
    	echo '<br /><br/>';
    }
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}


?>