<?php

include('_header.html');
include('_section.html');
?>
	<h2 class="center-align">Página del equipo</h2>
	<h5 class="center-align">En el siguiente apartado podrás consultar la opción que desees entre las que se muestran del equipo, es importante recordar que esta
información ha sido creada con finalidad puramente educativa.</h5>
	<br>
	<br>
	<div class="row">
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
				<a class="white-text" href="consulta1.php">Jugadores</a>
			</button>
		</div>
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
				<a class="white-text" href="consulta2.php">Peso</a>
			</button>
		</div>
		<div class="col s2">&nbsp</div>
		<div class="col s2">
			<button class="btn waves-effect waves-light red accent-4" type="submit" name="action">
				<a class="white-text" href="consulta3.php">ID</a>
			</button>
		</div>
	</div>
	<?php include ('_footer.html'); ?>