function cambios() {
	var x = document.getElementById("martin");
	if (x.value === "1") {
		document.getElementById("myP").style.fontFamily = "none";
	}
	if (x.value === "2") {
		document.getElementById("myP").style.fontFamily = "Helvetica";
	}
	if (x.value === "3") {
		document.getElementById("myP").style.fontStyle = "italic";
	}
	if (x.value === "4") {
		document.getElementById("myP").style.fontWeight = "bold";
	}
}

function allowDrop(ev) {
	ev.preventDefault();
}

function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	ev.target.appendChild(document.getElementById(data));
}