<?php

require_once('modelo.php');
include('_header.html');
include('_section.html');
?>
	<h2 class="center-align">Preguntas</h2>
	<br>
	<div class="container">
		<h5 class="center-align">1.- ¿Por qué es una buena práctica separar el modelo del controlador?</h5>
		<p align="justify">Porque en el modelo solo tenemos todas las funciones que implementaremos al programa y en el controlador solo tenemos que mandarlas a llamar cuando las necesitemos.</p>
		<h5 class="center-align">2.- ¿Qué es ODBC y para qué es útil?</h5>
		<p align="justify"><b>Open Data Base Conectivity</b> o lo que es lo mismo <b>conectividad abierta de bases de datos</b> es un elemento que por un lado es siempre igual, y por el otro es capaz de dialogar con una DB concreta, y es útil a la hora de utilizar bases de datos diferentes como Access o SQL pues solo tendríamos que ir cambiando este elemento, y nuestra aplicación siempre funcionaría sin importar lo que hay al otro lado.</p>
		<h5 class="center-align">3.- ¿Qué es SQL Injection?</h5>
		<p align="justify">La inyección directa de comandos SQL es una técnica donde un atacante crea o altera comandos SQL existentes para exponer datos ocultos, sobrescribir los valiosos, o peor aún, ejecutar comandos peligrosos a nivel de sistema en el equipo que hospeda la base de datos. </p>
		<h5 class="center-align">4.- ¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</h5>
		<p align="justify">Para <b>MySQL</b> existe la función <b>mysql_real_escape_string</b> la cuál evita la inyección, para <b>.NET</b> se convierte en literal los parametros ingresados con la función <b>SqlDbType.VarChar</b> y se evita la inyección, también existe la función <b>AddWithValue</b> la cual es similar a la anterior.</p>
	</div>
	<?php
include ('_footer.html');
?>