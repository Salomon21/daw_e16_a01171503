<?php
function connect_db(){
	$mysql = mysqli_connect ("localhost", "root","","hockey");
	return $mysql;
}
function close_db($mysql){
	mysqli_close($mysql);
}

function  getJugadores() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
	?>
	<table class="striped">
		<tr>
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Peso</td>
			<td>Funciones</td>
		</tr>
		<?php  while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)): ?>
			<tr>
				<td>
					<?php echo $row[1];   ?>
				</td>
				<td>
					<?php echo $row['apellido'];   ?>
				</td>
				<td>
					<?php echo ' ' .  $row['peso'].' kg.'; ?>
				</td>
				<td>
					<a href="editar.php?id=<?php echo $row['id']?>" class="btn-floating btn-tiny red btn modal-trigger" type="button" name="action">
						<i class="tiny material-icons">mode_edit</i>
					</a>
					<a href="borrar.php?id=<?php echo $row['id']?>" class="btn-floating btn-tiny red btn modal-trigger" type="button" name="action">
						<i class="tiny material-icons">delete</i>
					</a>
				</td>
			</tr>
			<?php endwhile;  ?>
	</table>

	<?php
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

function  getJugadoresA() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador WHERE peso>85'  ;
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    ?>
		<table class="striped">
			<tr>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>Peso</td>
			</tr>
			<?php
        
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        ?>
				<tr>
					<td>
						<?php echo $row[1];   ?>
					</td>
					<td>
						<?php echo $row['apellido'];   ?>
					</td>
					<td>
						<?php echo ' ' .  $row['peso'].' kg.'; ?>
					</td>
				</tr>
				<?php
    }
    ?>
		</table>
		<?php
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

function  getJugadoresI() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM jugador';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    ?>
			<table class="striped">
				<tr>
					<td>ID</td>
					<td>Nombre</td>
					<td>Apellido</td>
				</tr>
				<?php
        
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        ?>
					<tr>
						<td>
							<?php echo ' ' .  $row['id']; ?>
						</td>
						<td>
							<?php echo $row[1];   ?>
						</td>
						<td>
							<?php echo $row['apellido'];   ?>
						</td>

					</tr>
					<?php
    }
    ?>
			</table>
			<?php
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

function guardar($nombre, $apellido, $peso) {
   if ($peso < 0 || $peso == "" ) {
	   session_start();
	   $_SESSION["error"] = "El peso tiene que ser mayor que 0";
   } else  {
	$mysql = connect_db();
    
    // insert command specification 
    $query = 'INSERT INTO jugador (nombre,apellido,peso) VALUES (?,?,?)';
    // Preparing the statement 
    if (!($statement = $mysql->prepare($query))) {
        die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $nombre, $apellido, $peso)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
     // Executing the statement
     if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
      } 

    mysqli_free_result($results);
    
    close_db($mysql);
   }
}

?>