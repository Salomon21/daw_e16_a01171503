numerosNegativos=0;
numerosPositivos=0;
ceros=0;
var w = window.prompt("¿Cuántos números quieres poner en el arreglo?");
var arreglo = new Array(w);
for (i=0;i<w;i++){      
    arreglo[i]= window.prompt("Ingresa el número: ");
    if(arreglo[i]<0){
        numerosNegativos++;
    }
    if (arreglo[i]==0){
        ceros++;
    }
    if (arreglo[i]>0){
        numerosPositivos++;
    }
}
window.document.write("El arreglo es el siguiente: ["+arreglo+"]");
window.document.write("<br>");
window.document.write("Existen "+numerosNegativos+" números negativos en el arreglo.");
window.document.write("<br>");
window.document.write("Existen "+ceros+" números 0 en el arreglo.");
window.document.write("<br>");
window.document.write("Existen "+numerosPositivos+" números positivos en el arreglo.");
window.document.write("<br>");
