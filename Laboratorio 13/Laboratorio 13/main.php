<head>
	<meta charset="UTF-8">
	<title>Facebuu</title>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import css-->
	<link rel="stylesheet" href="../CSS/materialize.css">
	<link rel="stylesheet" href="../CSS/style.css">
	<!-- <link rel="font" href="font/roboto/"> -->

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
	<div class="container">
		&nbsp &nbsp &nbsp &nbsp

		<h1 class="white-text">FaceBuu</h1>
		<?php include("_form.html"); ?>
	</div>