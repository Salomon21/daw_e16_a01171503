<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Laboratorio 8 y 9</title>
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="CSS/materialize.css" media="screen,projection" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>
	<h1 class="center-align">Laboratorio 8 y 9</h1>
	<div class="container">
		<br>
		<h4 class="center-align">Formulario:</h4>
		<div class="row">
			<form class="col s12" action="servidor.php" method="post">
				<div class="row">
					<div class="input-field col s6">
						<input name="nombre" type="text" class="validate">
						<label for="first_name">Nombres: </label>
					</div>
					<div class="input-field col s6">
						<input name="apellidos" type="text" class="validate">
						<label for="last_name">Apellidos: </label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input name="email" type="email" class="validate">
						<label for="email">Email</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input name="password" type="password" class="validate">
						<label for="password">Contraseña</label>
					</div>
				</div>
				<button class="btn blue-grey darken-1 center-align" type="submit" name="action">Submit
					<i class="material-icons right">send</i>
				</button>
			</form>
		</div>
		<br>
		<h5>¿Por qué es una buena práctica separar el controlador de la vista?</h5>
		<p>Separar las funciones de la aplicación en modelos, vistas y controladores hace que la aplicación sea muy ligera. Estas características nuevas se añaden fácilmente y las antiguas toman automáticamente una forma nueva.</p>

		<h5>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h5>
		<p><strong>ECHO: </strong>Permite hacer output de todos los parámetros.
			<br> <strong>FGETS: </strong>Obtiene una línea de un file pointer.
			<br> <strong>CHMOD: </strong>Cambia el modo del archivo especificado al modo especificado.</p>
		<h5>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h5>
		<p><strong>Phpinfo: </strong>Es una función que muestra las extensiones añadidas a PHP y es muy útil para revisar su funcionalidad.
			<br><strong>Array_multisort: </strong>Permite ordenar en arreglos multidimensionales.</p>

	</div>
</body>

</html>