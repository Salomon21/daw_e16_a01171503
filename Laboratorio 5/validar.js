function val(){
    if(frm.password.value == ""){
        alert("Ingrese la contraseña.");
        frm.password.focus(); 
        return false;
    }
    if((frm.password.value).length < 6){
        alert("La contraseña no puede ser menor a 6 caracteres.");
        frm.password.focus();
        return false;
    }
    if(frm.confirmpassword.value == ""){
        alert("Ingrese la confirmación de la contraseña.");
        return false;
    }
    if(frm.confirmpassword.value != frm.password.value){
        alert("Las contraseñas no coinciden.");
        return false;
    }
    return true;
}